package xolova.divinerpg.utils.helpers.gui;

public class CreativeTabHelper
{
    public static CreativeTabTwilight tabSword = new CreativeTabTwilight();
    public static CreativeTabDivineBlock tabBlocks = new CreativeTabDivineBlock();
    public static CreativeTabItem tabItems = new CreativeTabItem();
    public static CreativeTabMobSpawners tabSpawner = new CreativeTabMobSpawners();
    public static CreativeTabRangedWeapons tabRanged = new CreativeTabRangedWeapons();
    public static CreativeTabLighting tabLighting = new CreativeTabLighting();
    public static CreativeTabUtility tabUtility = new CreativeTabUtility();
    public static CreativeTabHerbs tabHerb = new CreativeTabHerbs();
    public static CreativeTabArmor tabArmor = new CreativeTabArmor();
    public static CreativeTabTool tabTool = new CreativeTabTool();
    public static CreativeTabUnsorted tabUnsorted = new CreativeTabUnsorted();
}
