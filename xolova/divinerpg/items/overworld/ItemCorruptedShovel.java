package xolova.divinerpg.items.overworld;

import net.minecraft.item.EnumToolMaterial;
import xolova.divinerpg.items.core.ItemDivineRPGShovel;

public class ItemCorruptedShovel extends ItemDivineRPGShovel
{
    public ItemCorruptedShovel(int var1, EnumToolMaterial var2)
    {
        super(var1, var2);
    }
}
