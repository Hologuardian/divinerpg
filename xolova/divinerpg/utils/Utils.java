package xolova.divinerpg.utils;

public class Utils 
{
	public static final String VERSION = "1.4";
	
	public static final String MAIN_MOD_ID = "DivineRPG";
	public static final String MAIN_MOD_NAME = "DivineRPG";
	
	public static final String TWILIGHT_MOD_ID = "DivineRPGTwilight";
	public static final String TWLIGHT_MOD_NAME = "DivineRPG Twilight";
	
	public static final String ARCANA_MOD_ID = "DivineRPG|Arcana";
	public static final String ARCANA_MOD_NAME = "DivineRPG Arcana";
	
	public static final String ICEIKA_MOD_ID = "DivineRPGIceika";
	public static final String ICEIKA_MOD_NAME = "DivineRPG Iceika";
	
	public static final String VETHEA_MOD_ID = "DivineRPGVethea";
	public static final String VETHEA_MOD_NAME = "DivineRPG Vethea";

}
