package xolova.divinerpg.items.overworld;

import net.minecraft.item.EnumToolMaterial;

public class ItemCorruptedAxe extends ItemAxeXolovon
{
    public ItemCorruptedAxe(int par1, EnumToolMaterial par2EnumToolMaterial)
    {
        super(par1, par2EnumToolMaterial);
    }

    public String getTextureFile()
    {
        return "/Xolovon3.png";
    }
}
