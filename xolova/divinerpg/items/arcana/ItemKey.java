package xolova.divinerpg.items.arcana;

import xolova.divinerpg.items.ItemDivineRPG;

public class ItemKey extends ItemDivineRPG
{
    public ItemKey(int par1, int par2)
    {
        super(par1, par2);
    }

    public int getSheet()
    {
        return 4;
    }
}
