package xolova.divinerpg.blocks.arcana;

import net.minecraft.block.material.Material;
import xolova.divinerpg.blocks.BlockDivineRPG;

public class BlockEucalyptusPlanks extends BlockDivineRPG
{
    public BlockEucalyptusPlanks(int var1, int var2)
    {
        super(var1, var2, Material.wood);
    }
}
