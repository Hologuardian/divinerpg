package xolova.divinerpg.blocks.overworld;

import xolova.divinerpg.blocks.BlockDivineRPGLadder;

public class BlockAcceleraunch extends BlockDivineRPGLadder
{
    public BlockAcceleraunch(int var1)
    {
        super(var1);
        this.slipperiness = 3.0F;
    }
}
